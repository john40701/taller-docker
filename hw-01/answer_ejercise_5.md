Objetivo

La compañía para la que trabajáis estudia la posibilidad de incorporar a nivel interno una herramienta para la monitorización de logs. Para ello, os han encomendado la tarea de realizar una “Proof of Concept” (PoC). Tras evaluar diferentes productos, habéis considerado que una buena opción es la utilización del producto Elastic stack, cumple con los requisitos y necesidades de la empresa.

Tras comentarlo con el CTO a última hora de la tarde, os ha solicitado que preparéis una presentación para mañana a primera hora. Dado el escaso margen para montar la demostración, la opción más ágil y rápida es utilizar una solución basada en contenedores donde levantaréis el motor de indexación (ElasticSearch) y la herramienta de visualización (Kibana).

Rellena el siguiente fichero docker-compose para que podáis hacer la demostración al CTO.

R. Instrucciones para ejecutar el ejercicio:
        Se dispone del archivo:
                docker-compose.yml en la ruta /carpeta_5/docker-compose.yml 

        Dentro de la carpeta carpeta_5 se ejecutan los siguientes comandos:
                docker-compose up
                docker ps

Explicación pasó a paso del ejercicio:
    Se completa el docker-compose de acuerdo a los comentarios dados
     version: '3.6'
        services:
         elasticsearch:
          # Utilizar la imagen de elasticsearch v7.9.3
          image: docker.elastic.co/elasticsearch/elasticsearch:7.9.3
          # Asignar un nombre al contenedor
          container_name: elasticsearch
          # Define las siguientes variables de entorno:
          # discovery.type=single-node
          environment:
           - discovery.type=single-node
          # Emplazar el contenedor a la red de elastic
          networks:
           - elastic
         kibana:
          # Utilizar la imagen kibana v7.9.3
          image: docker.elastic.co/kibana/kibana:7.9.3
          # Asignar un nombre al contenedor
          container_name: kibana
          # Emplazar el contenedor a la red de elastic
          networks:
           - elastic
          # Define las siguientes variables de entorno:
          # ELASTICSEARCH_HOST=elasticsearch
          # ELASTICSEARCH_PORT=9200
          environment:
           ELASTICSEARCH_HOST: elasticsearch
           ELASTICSEARCH_PORT: 9200
          # Mapear el puerto externo 5601 al puerto interno 5601
          ports:
           - 5601:5601
          # El contenedor Kibana depe esperar a la disponibilidad del servicio elasticsearch
          depends_on:
           - elasticsearch
        networks:
          elastic:
            driver: bridge


        Se ejecuta:
            docker-compose up
            docker ps

                CONTAINER ID   IMAGE                                                 COMMAND                  CREATED             STATUS             PORTS                    NAMES
243900bf5bdb   docker.elastic.co/kibana/kibana:7.9.3                 "/usr/local/bin/dumb…"   About an hour ago   Up About an hour   0.0.0.0:5601->5601/tcp   kibana
65939493d3b0   docker.elastic.co/elasticsearch/elasticsearch:7.9.3   "/tini -- /usr/local…"   About an hour ago   Up About an hour   9200/tcp, 9300/tcp       elasticsearch

            Se ingresa a http://localhost:5601/app/home#/ y se verifica el paso a paso de acuerdo a las instrucciones del ejercicio.