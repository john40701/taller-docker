Indica la diferencia entre el uso de la instrucción CMD y ENTRYPOINT (Dockerfile)

R. CMD indica un comando por defecto a utilizar cuando se ejecuta el contenedor que puede ser sobrescrito si al momento de correr el contenedor se indica algún comando. Mientras que utilizando ENTRYPOINT el comando no será sobrescrito, útil cuando se requiere tener control de como se ejecuta el contendor.