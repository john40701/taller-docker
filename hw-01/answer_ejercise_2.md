Indica la diferencia entre el uso de la instrucción ADD y COPY (Dockerfile)

R. ADD al igual que el COPY permite copiar archivos desde el sistema de archivos local al contenedor, pero adicional permite que estos archivos provengan de URL o archivos comprimidos tgz.
Se recomienda utilizar COPY cuando estos archivos provengan del sistema de archivos local debido a que da más semántica a la operación.
