Objetivo
Crea una imagen docker a partir de un Dockerfile. Esta aplicación expondrá un servicio en el puerto 8080 y se deberá hacer uso de la instrucción HEALTHCHECK para comprobar si la aplicación está ofreciendo el servicio o por si el contrario existe un problema.

El healthcheck deberá parametrizarse con la siguiente configuración:
        • La prueba se realizará cada 45 segundos
        • Por cada prueba realizada, se esperará que la aplicación responda en menos de 5 segundos. Si tras 5 segundos no se obtiene respuesta, se considera que la prueba habrá fallado
        • Ajustar el tiempo de espera de la primera prueba (Ejemplo: Si la aplicación del contenedor tarda en iniciarse 10s, configurar el parámetro a 15s)
        • El número de reintentos será 2. Si fallan dos pruebas consecutivas, el contenedor deberá cambiar al estado “unhealthy”)

R. Instrucciones para ejecutar el ejercicio:
        Se dispone los archivos:
                dockerfile en la ruta /carpeta_4/Dockerfile 
                index.html en la ruta /carpeta_4/index.html

        Dentro de la carpeta carpeta_4 se ejecutan los siguientes comandos:
                docker build -t jr-ngnix-hc .
                docker run --name jr-3-nginx-run -d -p 8080:80 jr-ngnix-hc
                docker ps

Explicación pasó a paso del ejercicio:

        En la carpeta ejercise4 se dispone de un archivo Dockerfile con la configuración requerida del Healhcheck, se utiliza como base la configuración del ejercicio 3 y se agrega la configuración del healt check utilizando: HEALTHCHECK --interval=45s --timeout=5s --start-period=10s --retries=2 CMD curl -f http://localhost:80/ || exit 1

        Básicamente se va a verificar que el home responda de acuerdo a los parámetros establecidos.

        DockerFile :
                FROM nginx:1.19.3-alpine            
                EXPOSE 80                           
                VOLUME /static_conent               
                WORKDIR /usr/share/nginx/html/      
                COPY /index.html ./                 
                COPY /index.html /static_conent/  
                HEALTHCHECK --interval=45s --timeout=5s --start-period=10s --retries=2 CMD curl -f http://localhost:80/ || exit 1

        En la instrucción de healthcheckse ingresan los parámetros indicados en el requerimiento, adicional se verifica que la página de index
        Responda dentro de los parámetros indicados.

        Se ejecuta:
                docker build -t jr-ngnix-hc .
                docker run --name jr-3-nginx-run -d -p 8080:80 jr-ngnix-hc
                docker ps
                CONTAINER ID   IMAGE         COMMAND                  CREATED              STATUS                        PORTS                  NAMES
                44a0ef177b49   jr-ngnix-hc   "/docker-entrypoint.…"   About a minute ago   Up About a minute (healthy)   0.0.0.0:8082->80/tcp               jr-3-nginx-run