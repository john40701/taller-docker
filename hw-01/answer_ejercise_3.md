Objetivo
Crea un contenedor con las siguientes especificaciones:
a. Utilizar la imagen base NGINX haciendo uso de la versión 1.19.3
b. Al acceder a la URL localhost:8080/index.html aparecerá el mensaje HOMEWORK 1
c. Persistir el fichero index.html en un volumen llamado static_content

R. Instrucciones para ejecutar el ejercicio:

 Se dispone los archivos:
     dockerfile en la ruta /carpeta_3/Dockerfile 
     index.html en la ruta /carpeta_3/index.html
 Dentro de la carpeta carpeta_3 se ejecutan los siguientes comandos_
     docker build -t jr-ngnix .
     docker run --name jr-3-nginx-run -d -p 8080:80 jr-ngnix
     docker volume ls (Verificar si el volumen se creo correctamente)
     docker volume inspect <Nombre Volumen> (Verificar si el volumen se creó correctamente)
     docker exec -it jr-3-nginx-run sh
     # cd /static_content/ (Verificar si el volumen se creó correctamente dentro del contenedor)

Explicación pasó a paso del ejercicio:

A. Se crea el siguiente archivo docker ubicado en la carpeta ejercise_3 y se especifica la versión del nginx requerida:

        FROM nginx:1.19.3-alpine            -> Se especifica la versión del ngnix a utilizar (Objetivo a)
        EXPOSE 80                           -> Se indica que se el contenedor necesita exponer el puerto 80 (Objetivo b)
        VOLUME /static_conent               -> Se especifica el volumen a crear para persistir el archivo index html
        WORKDIR /usr/share/nginx/html/      -> Se especifica la ruta en donde se encuentran los html
        COPY /index.html ./                 -> Se copia el archivo index que se encuentra en la carpeta al ngnix  (Objetivo a)
        COPY /index.html /static_conent/    -> Se realiza una copia del html al volumen creado  (Objetivo c)

    
    Se construye la imagen
        docker build -t jr-ngnix .

B.  Ejecución del container en el puerto 8080 (Objetivo b)
        docker run --name jr-3-nginx-run -d -p 8080:80 jr-ngnix

    Se realiza la verificación, se habre un explorador en la ruta http://localhost:8080 y se verifica que ngnix levanto correctamente y el home tiene el texto deseado 

C. Para la copia del seguridad del archivo index.html, en el dockerfile creado en el punto a, se crea un volumen y se guarda ese archivo

    Se realiza verificación y se creó correctamente el volumen 
        
        docker volume ls

        DRIVER    VOLUME NAME
        local     f3108442d60af1733a370f9d7bfbc19d45ad33caad6a951f10aa9128541f3de6
                docker volume inspect f3108442d60af1733a370f9d7bfbc19d45ad33caad6a951f10aa9128541f3de6
                [
                    {
                        "CreatedAt": "2021-11-13T18:14:33Z",
                        "Driver": "local",
                        "Labels": null,
                        "Mountpoint": "/var/lib/docker/volumes/f3108442d60af1733a370f9d7bfbc19d45ad33caad6a951f10aa9128541f3de6/_data",
                        "Name": "f3108442d60af1733a370f9d7bfbc19d45ad33caad6a951f10aa9128541f3de6",
                        "Options": null,
                        "Scope": "local"
                    }
                ]

    Se verifica y el volumen fue creado correctamente en el contenedor
        
        docker exec -it jr-3-nginx-run sh
        /usr/share/nginx/html # cd ..
        /usr/share/nginx # cd ..
        /usr/share # cd ..
        /usr # ls
        bin    lib    local  sbin   share
        /usr # cd ..
        / # ls
        bin                   docker-entrypoint.sh  lib                   opt                   run                   static_content         usr
        dev                   etc                   media                 proc                  sbin                  sys                   var
        docker-entrypoint.d   home                  mnt                   root                  srv                   tmp
        / #

        y dentro del static_content se encuentra una copia del index.html
        / # cd static_content/
        /static_content # ls
            index.html

